import Foundation

enum ErrorMessages: String, Localizable {
    case noConnection = "ErrorMessages_noConnection"
    case genericError = "ErrorMessages_genericError"
}

enum LoginScreenStrings: String, Localizable {
    case emailIsEmpty = "LoginScreenStrings_emailIsEmpty"
    case passwordIsEmpty = "LoginScreenStrings_passwordIsEmpty"
    case invalidCredentials = "LoginScreenStrings_invalidCredentials"
}

enum HTTPErrorCasesStrings: String, Localizable {
    case unauthorized = "HTTPErrorCasesStrings_unauthorized"
    case internalServerError = "HTTPErrorCasesStrings_internalServerError"
    case generic = "HTTPErrorCasesStrings_generic"
}

enum HomeScreenStrings: String, Localizable {
    case searchResults = "HomeScreenStrings_searchResults"
    case resultNotFound = "HomeScreenStrings_resultNotFound"
    case allCategories = "HomeScreenStrings_allCategories"
}
