//
//  UITextField + Extensions.swift
//  companies-app-ios
//
//  Created by Brenner on 03/12/20.
//

import UIKit

extension UITextField {
    
    func applyErrorAppearance() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.brandRed?.cgColor
        self.layer.cornerRadius = 5
    }
    
    func defaultTextFieldState() {
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.brandRed?.cgColor
        self.layer.cornerRadius = 5
    }
}
