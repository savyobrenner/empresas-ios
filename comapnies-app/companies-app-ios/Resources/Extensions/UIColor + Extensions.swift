//
//  UIColor + Extensions.swift
//  companies-app-ios
//
//  Created by Brenner on 03/12/20.
//

import UIKit

extension UIColor {
    
    static let brandPink = UIColor(named:"brandPink")
    static let brandGray = UIColor(named:"brandGray")
    static let brandRed = UIColor(named:"brandRed")
    static let brandLoadingColor = UIColor(named: "brandLoadingColor")!
}
