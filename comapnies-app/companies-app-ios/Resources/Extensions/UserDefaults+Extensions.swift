//
//  UserDefaults+Extensions.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation

extension UserDefaults {
    
    enum Keys: String {
        
        case accessToken
        case client
        case uid
        
        var description: String {
            return self.rawValue
        }
    }
}

