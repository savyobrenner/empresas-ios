//
//  UI + Extensions.swift
//  companies-app-ios
//
//  Created by Brenner on 03/12/20.
//

import UIKit


extension UIImageView {
    
    func applySemiCircleEffect() {
        guard let image = self.image else {
            return
        }

        let size = image.size

        self.clipsToBounds = true
        self.image = image

        let curveRadius    = size.width * 0.006 // Adjust curve of the image view here
        let invertedRadius = 1.0 / curveRadius

        let rect = CGRect(x: 0,
                          y: -40,
                      width: self.bounds.width + size.width * 2 * invertedRadius,
                     height: self.bounds.height)

        let ellipsePath = UIBezierPath(ovalIn: rect)
        let transform = CGAffineTransform(translationX: -size.width * invertedRadius, y: 0)
        ellipsePath.apply(transform)

        let rectanglePath = UIBezierPath(rect: self.bounds)
        rectanglePath.apply(CGAffineTransform(translationX: 0, y: -size.height * 0.5))
        ellipsePath.append(rectanglePath)

        let maskShapeLayer   = CAShapeLayer()
        maskShapeLayer.frame = self.bounds
        maskShapeLayer.path  = ellipsePath.cgPath
        self.layer.mask = maskShapeLayer
    }
}
