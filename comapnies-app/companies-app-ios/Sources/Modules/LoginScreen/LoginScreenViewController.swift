//  
//  LoginScreenViewController.swift
//  companies-app-ios
//
//  Created by Brenner on 03/12/20.
//

import UIKit

class LoginScreenViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var topBackground: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailErrorToggle: UIButton!
    @IBOutlet weak var passwordToggle: UIButton!
    @IBOutlet weak var errorCredentialsLabel: UILabel!
    @IBOutlet weak var welcomeCompaniesLabel: UILabel!
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    
    
    // MARK: - Properties
    var presenter: LoginScreenPresenter!
    var isToggleActive = false
    var isCredentialsValid: Bool? {
        didSet {
            guard let conditional = isCredentialsValid else { return }
            if !conditional {
                setupLayoutForInvalidCredentials(isEmail: false, isPassword: false, isBoth: true)
            } else {
                setupLayoutForDefaultState()
            }
        }
    }
    
    
    // MARK: - View Lifecycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Methods
    func initialSetup() {
        topBackground.applySemiCircleEffect()
        setupLayoutForDefaultState()
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func setupLayoutForInvalidCredentials(isEmail: Bool, isPassword: Bool, isBoth: Bool) {
        
        if isEmail {
            emailErrorToggle.isHidden = false
            errorCredentialsLabel.isHidden = false
            errorCredentialsLabel.text = LoginScreenStrings.emailIsEmpty.localized()
            emailTextField.applyErrorAppearance()
        } else if isPassword {
            passwordToggle.tintColor = UIColor.brandRed
            passwordToggle.setImage(UIImage.init(systemName: "xmark.circle.fill"), for: .normal)
            errorCredentialsLabel.isHidden = false
            errorCredentialsLabel.text = LoginScreenStrings.passwordIsEmpty.localized()
            passwordTextField.applyErrorAppearance()
        } else if isBoth {
            emailErrorToggle.isHidden = false
            errorCredentialsLabel.isHidden = false
            errorCredentialsLabel.text = LoginScreenStrings.invalidCredentials.localized()
            passwordToggle.tintColor = UIColor.brandRed
            passwordToggle.setImage(UIImage.init(systemName: "xmark.circle.fill"), for: .normal)
            passwordTextField.applyErrorAppearance()
            emailTextField.applyErrorAppearance()
        }
    }
    
    func setupLayoutForDefaultState() {
        emailErrorToggle.isHidden = true
        errorCredentialsLabel.isHidden = true
        passwordToggle.tintColor = UIColor.brandGray
        passwordToggle.setImage(UIImage.init(systemName: "eye.fill"), for: .normal)
        passwordTextField.defaultTextFieldState()
        emailTextField.defaultTextFieldState()
    }
    
    func setupLayoutForStartInputCredentials() {
        imageTopConstraint.constant = -250
        welcomeCompaniesLabel.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func validateTextFields() {
        guard let email = emailTextField.text, !email.isEmpty else {
            setupLayoutForInvalidCredentials(isEmail: true, isPassword: false, isBoth: false)
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            setupLayoutForInvalidCredentials(isEmail: false, isPassword: true, isBoth: false)
            return
        }
        
        presenter.authenticate(email: email, password: password)
    }
    
    
    // MARK: - Actions
    
    @IBAction func toggleEyePassword(_ sender: Any) {
        if !isToggleActive {
            isToggleActive = true
            passwordTextField.isSecureTextEntry = false
        } else {
            isToggleActive = false
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    @IBAction func signIn(_ sender: Any) {
        validateTextFields()
    }
    
    
    
}

// MARK: - LoginScreenPresenterDelegate
extension LoginScreenViewController: LoginScreenPresenterDelegate {
    func invalidCredentials() {
        isCredentialsValid = false
    }
}

extension LoginScreenViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isCredentialsValid = true
        setupLayoutForStartInputCredentials()
    }
}
