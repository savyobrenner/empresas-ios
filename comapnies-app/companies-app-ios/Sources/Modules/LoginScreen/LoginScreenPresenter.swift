//  
//  LoginScreenPresenter.swift
//  companies-app-ios
//
//  Created by Brenner on 03/12/20.
//

import Foundation

protocol LoginScreenPresenterDelegate: BasePresenterDelegate {
    func invalidCredentials()
}

class LoginScreenPresenter {
    
    weak var delegate: LoginScreenPresenterDelegate?
    let router: LoginScreenRouter
    
    init(delegate: LoginScreenPresenterDelegate, router: LoginScreenRouter) {
        
        self.delegate = delegate
        self.router = router
    }
    
    func authenticate(email: String, password: String) {
        delegate?.showLoader()
        APIAuthentication.authenticate(withEmail: email, withPassword: password) {
            self.delegate?.hideLoader()
            self.router.navigateToHome()
        } failure: { (error) in
            let codeState = HTTPErrorCases(code: error.code)
            switch codeState {
            case .unauthorized:
                self.delegate?.hideLoader()
                self.delegate?.invalidCredentials()
            default:
                self.delegate?.hideLoader()
                self.delegate?.showFailure(codeState.message)
                return
            }
        }

    }
}
