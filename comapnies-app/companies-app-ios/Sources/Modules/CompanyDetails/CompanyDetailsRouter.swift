//  
//  CompanyDetailsRouter.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import UIKit

class CompanyDetailsRouter: BaseRouter {

    static func makeModule(_ enterprise: Enterprise?) -> UIViewController {
        
        let viewController = CompanyDetailsViewController()
        let router = CompanyDetailsRouter(viewController: viewController)
        let presenter = CompanyDetailsPresenter(delegate: viewController, router: router, enterprise: enterprise)
        viewController.presenter = presenter
        
        return viewController
    }
}
