//  
//  CompanyDetailsPresenter.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import Foundation

protocol CompanyDetailsPresenterDelegate: BasePresenterDelegate {
    func loadInformations(_ enterprise: Enterprise?)
}

class CompanyDetailsPresenter {
    
    weak var delegate: CompanyDetailsPresenterDelegate?
    let router: CompanyDetailsRouter
    var enterprise: Enterprise?
    
    init(delegate: CompanyDetailsPresenterDelegate, router: CompanyDetailsRouter, enterprise: Enterprise?) {
        
        self.delegate = delegate
        self.router = router
        self.enterprise = enterprise
    }
    
    func didLoad() {
        delegate?.loadInformations(enterprise)
    }
    
    func willAppear() {
    }
    
    func didAppear() {
    }
}
