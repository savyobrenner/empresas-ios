//
//  CategoriesCell.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryLabel: RoundLabel!
    
    
    var buttonTitle: String? {
        didSet {
            categoryLabel.text = buttonTitle ?? ""
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                categoryLabel.backgroundColor = .gray
                categoryLabel.textColor = .white
            } else {
                categoryLabel.backgroundColor = UIColor.brandLoadingColor
                categoryLabel.textColor = UIColor.brandPink
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
