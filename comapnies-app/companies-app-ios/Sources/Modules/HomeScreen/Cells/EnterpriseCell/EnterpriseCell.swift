//
//  EnterpriseCell.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import UIKit
import Kingfisher

class EnterpriseCell: UITableViewCell {
    
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyTItle: UILabel!
    
    var enterprise: Enterprise? {
        didSet {
            guard let photoUrl = enterprise?.photo, let name = enterprise?.enterprise_name else { return }
            let finalPhotoUrl = URL(string: EndPoint.loadImage.fullPath + photoUrl)
            companyTItle.text = name
            companyImage.kf.setImage(with: finalPhotoUrl)
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        companyImage.kf.indicatorType = .activity
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
