//  
//  HomeScreenPresenter.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation

protocol HomeScreenPresenterDelegate: BasePresenterDelegate {
    func loadEnterprises(enterprises: [Enterprise?])
    func loadEnterprisesCategory(categories: [EnterpriseType?])
    func loadAllEnterprises(enterprises: [Enterprise?])
}

class HomeScreenPresenter {
    
    weak var delegate: HomeScreenPresenterDelegate?
    let router: HomeScreenRouter
    
    init(delegate: HomeScreenPresenterDelegate, router: HomeScreenRouter) {
        self.delegate = delegate
        self.router = router
    }
    
    func getEnterprisesByFilter(keyword: String, category: Int?) {
        APIGetEnterprises.getEnterprisesByFilter(withKeyword: keyword, type: category) { (result) in
            
            guard let enterprises = result?.enterprises else {
                self.delegate?.showFailure(ErrorMessages.genericError.localized())
                return
            }
            
            self.delegate?.loadEnterprises(enterprises: enterprises)
            
        } failure: { (error) in
            self.delegate?.showFailure(ErrorMessages.genericError.localized())
        }
    }
    
    func getAllEnterprises(id: Int?, isFirstRequest: Bool) {
        APIGetEnterprises.getAllEnterprises(id: id) { (result) in
            
            guard let enterprises = result?.enterprises else {
                self.delegate?.showFailure(ErrorMessages.genericError.localized())
                return
            }
            
            if !isFirstRequest {
                self.delegate?.loadAllEnterprises(enterprises: enterprises)
                return
            }
            
            self.delegate?.loadAllEnterprises(enterprises: enterprises)
            
            var enterprisesType: [EnterpriseType?] = []
            let firstValue = EnterpriseType(id: nil, enterprise_type_name: HomeScreenStrings.allCategories.localized())
            enterprisesType.append(firstValue)
            
            for enterprise in enterprises {
                let alreadyContains = enterprisesType.contains { (object) -> Bool in
                    if object?.enterprise_type_name == enterprise?.enterprise_type?.enterprise_type_name {
                        return true
                    }
                    return false
                }
                
                if !alreadyContains {
                    enterprisesType.append(enterprise?.enterprise_type)
                }
            }
            
            let sortedCategories =  enterprisesType.sorted { (firstObject, secondObject) -> Bool in
                if (firstObject?.enterprise_type_name)! < (secondObject?.enterprise_type_name)! {
                    return true
                }
                return false
            }
            self.delegate?.loadEnterprisesCategory(categories: sortedCategories)
            
        } failure: { (error) in
            self.delegate?.showFailure(ErrorMessages.genericError.localized())
        }
        
    }
}
