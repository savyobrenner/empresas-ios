//
//  APIAuthentication.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation
import Alamofire

struct APIAuthentication {
    
    static func authenticate(withEmail email: String, withPassword password: String, success: @escaping () -> Void, failure: @escaping (ResponseError) -> Void) {
        
        let request = SignInRequest(email: email, password: password)
        
        Network.sharedInstance.request(request: request) { (response: SignInResponse) in
            
            guard response.success() else {
                failure(response.responseError())
                return
            }
            success()
        }
        
    }
}

