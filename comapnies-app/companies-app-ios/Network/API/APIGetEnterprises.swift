//
//  APIGetEnterprises.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import Foundation

struct APIGetEnterprises {
    
    static func getEnterprisesByFilter(withKeyword keyword: String, type enterpriseType: Int?, success: @escaping (Enterprises?) -> Void, failure: @escaping (ResponseError) -> Void) {
        
        let request = GetEnterpriseByFilterRequest(filterKey: keyword, enterpriseType: enterpriseType)
        
        Network.sharedInstance.request(request: request) { (response: GetEnterpriseByFilterResponse) in
            
            guard response.success() else {
                failure(response.responseError())
                return
            }
            
            success(response.enterprises)
        }
        
    }
    
    static func getAllEnterprises(id: Int?, success: @escaping (Enterprises?) -> Void, failure: @escaping (ResponseError) -> Void) {
        
        let request = GetAllEnterprisesRequest(id)
        
        Network.sharedInstance.request(request: request) { (response: GetAllEnterprisesResponse ) in
            guard response.success() else {
                failure(response.responseError())
                return
            }
            
            success(response.enterprises)
        }
    }
}


