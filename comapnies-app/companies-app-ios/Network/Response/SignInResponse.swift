//
//  SignInResponse.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation

class SignInResponse: ResponseHandler {
    
    var userAuthentication = UserAuthentication()
    
    override func parseModel() throws {
        if success() {
            guard let response = urlResponse?.headers else { return }
            userAuthentication.accessToken = response.value(for: "access-token")
            userAuthentication.client = response.value(for: "client")
            userAuthentication.uid = response.value(for: "uid")
            UserDefaults.standard.setValue(userAuthentication.accessToken, forKey: UserDefaults.Keys.accessToken.description)
            UserDefaults.standard.setValue(userAuthentication.client, forKey: UserDefaults.Keys.client.description)
            UserDefaults.standard.setValue(userAuthentication.uid, forKey: UserDefaults.Keys.uid.description)
        }
    }
    
}
