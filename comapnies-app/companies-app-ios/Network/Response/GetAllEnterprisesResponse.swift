//
//  GetAllEnterprisesResponse.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import Foundation

class GetAllEnterprisesResponse: ResponseHandler {
    
    var enterprises: Enterprises?
    
    override func parseModel() throws {
        if success() {
            guard let data = data else { return }
            enterprises = try JSONDecoder().decode(Enterprises.self, from: data)
        }
    }
    
}
