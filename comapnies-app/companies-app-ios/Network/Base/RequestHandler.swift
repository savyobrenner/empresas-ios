//
//  RequestHandler.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation
import  Alamofire

class RequestHandler {
    
    var data: Data?
    
    func path() -> String {
        preconditionFailure("This method must be overridden")
    }
    
    func httpMethod() -> RequestMethod {
        preconditionFailure("This method must be overridden")
    }
    
    func headers() -> HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
    
    public init() {}
    
    func parameters() -> [String: Any]? {
        preconditionFailure("This method must be overridden")
    }
    
    func encoding() -> EncodingStrategy {
        return .JSON
    }
}

enum EncodingStrategy {
    case JSON
    case URL
}

extension EncodingStrategy {
    var alamofireEncoding: ParameterEncoding {
        switch self {
        case .JSON:  return JSONEncoding()
        case .URL: return URLEncoding(boolEncoding: .literal)
        }
    }
}
