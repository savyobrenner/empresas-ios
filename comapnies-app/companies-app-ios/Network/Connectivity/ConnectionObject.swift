//
//  ConnectionObject.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Alamofire
import Foundation

class ConnectionObject {
    static let sharedInstance = ConnectionObject()
    
    var networkManager: NetworkReachabilityManager?
    
    init() {
        self.networkManager = NetworkReachabilityManager()
    }
    
    func isConnectedToInternet() -> Bool {
        guard let manager = self.networkManager else { return false }
        return manager.isReachable
    }
}

