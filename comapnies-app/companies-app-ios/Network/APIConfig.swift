//
//  APIConfig.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation
import Alamofire

enum EndPoint {
    case signIn
    case getEnterpriseByFilter
    case loadImage
}

enum Version: String {
    case v1
    case v2
    case v3
}

extension EndPoint {
    
    private var baseURL: String { return "https://empresas.ioasys.com.br" }
    
    private var version: Version { return .v1}
    
    private var path: String {
        switch self {
        case .signIn:
            return "/api/\(version.rawValue)/users/auth/sign_in"
        case .getEnterpriseByFilter:
            return "/api/\(version.rawValue)/enterprises"
        case .loadImage:
            return "/"
        }
    }
    
    var fullPath: String { return baseURL+path }
}

enum RequestMethod {
    case get
    case post
    case put
    case delete
}

extension RequestMethod {
    var alamofireMethod: HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        case .delete:
            return .delete
        }
    }
}

