//
//  UserAuthentication.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation

struct UserAuthentication: Codable {
    var accessToken: String?
    var uid: String?
    var client: String?
}
