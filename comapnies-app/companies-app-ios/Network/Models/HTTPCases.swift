//
//  HTTPCases.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation

enum HTTPErrorCases: Int {
    case unauthorized = 401
    case internalServerError = 500
    case generic = -1
    case noConnection = -25
    
    init(code: Int) {
        
        switch code {
        case 401: self = .unauthorized
        case 500 : self = .internalServerError
        case -25: self = .noConnection
        default:
            self = .generic
        }
        
    }
    
    var message: String {
        switch self {
        case .unauthorized:
            return HTTPErrorCasesStrings.unauthorized.localized()
        case .internalServerError:
            return HTTPErrorCasesStrings.internalServerError.localized()
        case .noConnection:
            return ErrorMessages.noConnection.localized()
        case .generic:
            return HTTPErrorCasesStrings.generic.localized()
        }
    }
}
