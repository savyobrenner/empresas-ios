//
//  GetEnterpriseByFilterRequest.swift
//  companies-app-ios
//
//  Created by Brenner on 05/12/20.
//

import Foundation
import Alamofire

class GetEnterpriseByFilterRequest: RequestHandler {
    
    let filterKey: String
    let enterpriseType: Int?
    
    init(filterKey: String, enterpriseType: Int?) {
        self.filterKey = filterKey.replacingOccurrences(of: " ", with: "%20")
        self.enterpriseType = enterpriseType
        super.init()
    }
    
    override func path() -> String {
        if let enterpriseType = enterpriseType {
            return EndPoint.getEnterpriseByFilter.fullPath+"?enterprise_types=\(enterpriseType)&name=\(filterKey)"
        } else {
            return EndPoint.getEnterpriseByFilter.fullPath+"?name=\(filterKey)"
        }
    }
    
    override func httpMethod() -> RequestMethod {
        return .get
    }
    
    override func headers() -> HTTPHeaders? {
    guard let accessToken = UserDefaults.standard.value(forKey: UserDefaults.Keys.accessToken.description), let client = UserDefaults.standard.value(forKey: UserDefaults.Keys.client.description), let uid = UserDefaults.standard.value(forKey: UserDefaults.Keys.uid.description) else { return nil }
        return ["Content-Type" : "application/json",
                "access-token" : "\(accessToken)",
                "client" : "\(client)",
                "uid" : "\(uid)"]
    }
    
    override func parameters() -> [String : Any]? {
        return nil
    }
    
}
