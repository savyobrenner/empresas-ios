//
//  SignInRequest.swift
//  companies-app-ios
//
//  Created by Brenner on 04/12/20.
//

import Foundation

class SignInRequest: RequestHandler {
    
    let email: String
    let password: String
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
        super.init()
    }
    
    override func path() -> String {
        return EndPoint.signIn.fullPath
    }
    
    override func httpMethod() -> RequestMethod {
        return .post
    }
    
    override func parameters() -> [String : Any]? {
        return ["email" : email, "password" : password]
    }
    
}
